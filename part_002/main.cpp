/*!
 * \file    main.cpp
 * \brief   SDL Tutorial. Create a fullscreen graphic window filled change color from blue to green
 * \author  Philippe Lucidarme (www.lucidarme.me)
 * \version 1.0
 * \date    18/01/2017
 */
#include <SDL2/SDL.h>
#include <iostream>
#include <stdint.h>


int main(/*int argc, char* argv[]*/)
{


    // ______________________
    // ::: Initialize SDL :::
    // ______________________

    // Initialize SDL for video
    if (SDL_Init(SDL_INIT_VIDEO)<0)
    {
        std::cerr << "SDL could not initialize! SDL_Error: " <<  SDL_GetError() << std::endl;
        return -1;
    }


    // ______________________________
    // ::: Get desktop resolution :::
    // ______________________________

    // Get desktop resolution
    SDL_DisplayMode currentMode;
    if (SDL_GetCurrentDisplayMode(0, &currentMode)!=0)
    {
        std::cerr << "Error while getting current display mode ! SDL_Error: " <<  SDL_GetError() << std::endl;
        return -1;
    }


    // _________________________________
    // ::: Create full screen window :::
    // _________________________________

    //Create window
    SDL_Window* window = NULL;
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, currentMode.w, currentMode.h, SDL_WINDOW_SHOWN );
    if( window == NULL )
    {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return -1;
    }


    // Use the desktop's format and refresh rate
    SDL_SetWindowDisplayMode(window,NULL);
    // Switch to full screen
    SDL_SetWindowFullscreen(window,SDL_WINDOW_FULLSCREEN);



    // ___________________________________
    // ::: Fade out from blue to green :::
    // ___________________________________

    //Get window surface
    SDL_Surface* screenSurface = NULL;
    screenSurface = SDL_GetWindowSurface( window );

    // Loop for colors
    for (int i=0;i<0xFF;i++)
    {
        // The window color starts with blue and fade to green
        SDL_FillRect( screenSurface, NULL, SDL_MapRGB( screenSurface->format, 0x00, i, 0xFF-i ) );

        //Update the surface
        SDL_UpdateWindowSurface( window );

        //Wait 5 milliseconds
        SDL_Delay( 5 );
    }

    // _____________________________________
    // ::: Clean up and exit the program :::
    // _____________________________________

    SDL_DestroyWindow( window );
    SDL_Quit();
    return 0;

}
