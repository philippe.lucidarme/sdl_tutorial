/*!
 * \file    main.cpp
 * \brief   SDL Tutorial. Get and echo the properties of each display
 * \author  Philippe Lucidarme (www.lucidarme.me)
 * \version 1.0
 * \date    18/01/2017
 */
#include <SDL2/SDL.h>
#include <iostream>
#include <stdint.h>




int main(/*int argc, char* argv[]*/)
{

    // Initialize SDL for video
    if (SDL_Init(SDL_INIT_VIDEO)<0)
    {
        std::cerr << "SDL could not initialize! SDL_Error: " <<  SDL_GetError() << std::endl;
        return -1;
    }

    // Get the number of display(s)
    std::cout << SDL_GetNumVideoDisplays() << " display(s) on this computer" << std::endl;

    // For each screen
    for (int32_t nDisplay=0 ; nDisplay<SDL_GetNumVideoDisplays() ; nDisplay++)
    {
        // Display number of video mode
        std::cout << "::: " << SDL_GetNumDisplayModes(nDisplay) << " mode(s) for display #" << nDisplay << " :::" << std::endl;




        // _______________________________________
        // ::: Display all the available modes :::
        // _______________________________________

        for (int32_t nMode=0 ; nMode<SDL_GetNumDisplayModes(nDisplay) ; nMode ++)
        {
            SDL_DisplayMode mode;
            if (SDL_GetDisplayMode(nDisplay,nMode,&mode)==0)
            {
                // Display mode info (resolution, refresh rate, bpp ...)
                std::cout << "- Mode #" << nMode;
                std::cout << "\t" << mode.w << "x" << mode.h;
                std::cout << "\t" << mode.refresh_rate << "Hz";
                std::cout << "\t" << SDL_BITSPERPIXEL(mode.format) << " bpp ";
                std::cout << "(" << SDL_GetPixelFormatName(mode.format)  << ")" << std::endl;

            }
            else
            {
                // Error while getting mode info
                std::cerr << "Error while getting display mode for display #" << nDisplay << ", mode #" << nMode << "(" << SDL_GetError() << ")" << std::endl;
            }
        }



        // ________________________________
        // ::: Display the current mode :::
        // ________________________________


        SDL_DisplayMode currentMode;
        if (SDL_GetCurrentDisplayMode(nDisplay, &currentMode)==0)
        {
            // Display mode info (resolution, refresh rate, bpp ...)
            std::cout << "-> Current";
            std::cout << "\t" << currentMode.w << "x" << currentMode.h;
            std::cout << "\t" << currentMode.refresh_rate << "Hz";
            std::cout << "\t" << SDL_BITSPERPIXEL(currentMode.format) << " bpp ";
            std::cout << "(" << SDL_GetPixelFormatName(currentMode.format)  << ")" << std::endl;
        }
        else
        {
            // Error while getting current mode info
            std::cerr << "Error while getting display mode for display #" << nDisplay << ", current mode" << std::endl;
        }

    }


    // Clean up and exit the program.
    SDL_Quit();
    return 0;

}
