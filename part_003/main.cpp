/*!
 * \file    main.cpp
 * \brief   SDL Tutorial. Create a a window and display a PNG image inside
 * \author  Philippe Lucidarme (www.lucidarme.me)
 * \version 1.0
 * \date    18/01/2017
 */
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>


//Screen dimension constants
#define     WINDOW_WIDTH        800
#define     WINDOW_HEIGHT       600
// Image to display
#define     IMAGE_FILENAME      "../SDL_logo.png"




int main( /* int argc, char* args[] */ )
{
    // ______________________
    // ::: Initialize SDL :::
    // ______________________

    // Initialize SDL for video
    if (SDL_Init(SDL_INIT_VIDEO)<0)
    {
        std::cerr << "SDL could not initialize! SDL_Error: " <<  SDL_GetError() << std::endl;
        return -1;
    }

    // Set texture filtering to linear
    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
    {
        std::cerr << "Warning: Linear texture filtering not enabled!" << std::endl;
    }


    // _____________________
    // ::: Create window :::
    // _____________________

    //Create window
    SDL_Window* window = NULL;
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN );
    if( window == NULL )
    {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return -1;
    }


    // _______________________
    // ::: Create renderer :::
    // _______________________

    //Create renderer for window
    //The window renderer
    SDL_Renderer* renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
    if( renderer == NULL )
    {
        std::cerr << "Renderer could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return -1;
    }
    //Initialize renderer background color
    SDL_SetRenderDrawColor( renderer, 0x00, 0x00, 0xFF, 0xFF );



    // ______________________________
    // ::: Initialize PNG loading :::
    // ______________________________

    //Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        std::cerr << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
        return -1;
    }


    // __________________
    // ::: Load image :::
    // __________________

    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( IMAGE_FILENAME );
    if( loadedSurface == NULL )
    {
        std::cerr << "Error while loading image. Error :  " << IMG_GetError() << std::endl;
        return -1;
    }


    // _______________________________________
    // ::: Create a texture from the image :::
    // _______________________________________

    //Create texture from surface pixels
    SDL_Texture* texture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
    if( texture == NULL )
    {
        std::cerr << "Error while creating texture from image. Error :  " << SDL_GetError() << std::endl;
        return -1;
    }

    // Free memory, loadedSurface is no longer used
    SDL_FreeSurface( loadedSurface );




    //Main loop flag
    bool quit = false;



    //While application is running
    do
    {
        //Handle events on queue
        SDL_Event e;
        while( SDL_PollEvent( &e ) != 0 )
        {
            //User requests quit or escape is pressed
            if( e.type == SDL_QUIT ) quit = true;
            if (e.type == SDL_KEYDOWN && e.key.keysym.sym==SDLK_ESCAPE) quit = true;
        }

        //Clear screen
        SDL_RenderClear( renderer );
        SDL_Rect DestR;


        DestR.x = 0;
        DestR.y = 0;
        DestR.w = 300;
        DestR.h = 200;

        //Render texture to screen
        SDL_RenderCopy( renderer, texture, NULL, &DestR );

        //Update screen
        SDL_RenderPresent( renderer );
    }
    while( !quit );


    // _____________________________________
    // ::: Clean up and exit the program :::
    // _____________________________________

    // Destroy SDL items (free memory)
    SDL_DestroyTexture( texture );
    SDL_DestroyRenderer( renderer );
    SDL_DestroyWindow( window );

    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();

    return 0;
}

