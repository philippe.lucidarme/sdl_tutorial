QT += core
QT -= gui

TARGET = part_003
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp



# Create directories for temporrary files
OBJECTS_DIR     = tmp
MOC_DIR         = tmp

# Binaries will be placed in the bin directory
DESTDIR         = bin

# We use the SDL (Simple DirectMedia Layer)
# Packages :
# sudo apt install libsdl2-dev
# sudo apt install libsdl2-image-dev
LIBS            += -L/usr/local/lib
LIBS            += -lSDL2
LIBS            += -lSDL2_image
